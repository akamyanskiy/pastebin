from django.conf.urls import patterns, include, url
from django.views.generic.list_detail import object_detail, object_list
from django.views.generic.create_update import create_object
from pastebin.models import Paste


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

display_info = {'queryset': Paste.objects.all()}
create_info = {'model': Paste}



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pastein.views.home', name='home'),
    # url(r'^pastein/', include('pastein.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^paste/$', object_list, dict(display_info, allow_empty=True)),
    url(r'^paste/(?P<object_id>\d+)/$', object_detail, display_info),
    url(r'^paste/add/$', create_object, create_info),
)
